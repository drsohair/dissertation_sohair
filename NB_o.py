import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import string
import re
from wordcloud import WordCloud, STOPWORDS , ImageColorGenerator
import spacy
from collections import Counter
from spacy.lang.en.stop_words import STOP_WORDS
import csv
from spacy.tokens import Span
from spacy.tokens import Doc
spacy_stopwords = spacy.lang.en.stop_words.STOP_WORDS
nlp = spacy.load("en_core_web_sm")
nlp.Defaults.stop_words|={"oh","amp",}
nlp.Defaults.stop_words|={"nt","na","amp"}
from sklearn.base import TransformerMixin
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import MultinomialNB
from spacy.lang.en import English
from sklearn import metrics
from sklearn.feature_extraction.text import CountVectorizer,TfidfVectorizer
from sklearn.metrics import accuracy_score 
from sklearn.metrics import classification_report, confusion_matrix
import pickle
from sklearn. model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import permutation_test_score
import pickle
import joblib
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import mutual_info_classif

filename = 'top_features_negative_class.csv'

stopwords = list(STOP_WORDS)
punct = string.punctuation

parser = English()

df_all=pd.read_csv("Data_all_O.csv" ,skipinitialspace = True)

# Code has been built  with the help of the following kaggle 
# links: https://www.kaggle.com/code/satishgunjal/tutorial-text-classification-using-spacy
# https://www.dataquest.io/blog/tutorial-text-classification-in-python-using-spacy/
# This site was a great help https://stackoverflow.com/questions
#Custom transformer using spaCy 
class predictors(TransformerMixin):
    def transform(self, X, **transform_params):
        return [clean_text(text) for text in X]
    def fit(self, X, y=None, **fit_params):
        return self
    def get_params(self, deep=True):
        return {}

# Basic function to clean the text 
def clean_text(text):     
    return text.strip().lower()

def text_data_cleaning(sentence):
    doc = nlp(sentence)
    
    tokens = []
    for token in doc:
        if token.lemma_ != "-PRON-":
            temp = token.lemma_.lower().strip()
        else:
            temp = token.lower_
        tokens.append(temp)
    
    cleaned_tokens = []
    for token in tokens:
        if token not in stopwords and token not in punct and token.isalpha()and token != 'm':
            cleaned_tokens.append(token)
    return cleaned_tokens



tfidf = TfidfVectorizer(tokenizer = text_data_cleaning)

bow_vector = CountVectorizer(tokenizer = text_data_cleaning, ngram_range=(1,1))

#print(bow_vector)

classifier = MultinomialNB()

X = df_all['text'] # the features we want to analyze
#print(df_all.head())
y  = df_all['lable'] # the labels, or answers, we want to test against

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 42)
X_train.shape, X_test.shape


clf = Pipeline([("cleaner", predictors()),('tfidf', tfidf), ('clf', classifier)])
#clf = Pipeline([("cleaner", predictors()),('counter_vectoriser', bow_vector), ('clf', classifier),])
clf.fit(X_train, y_train)



# get the feature names from the TfidfVectorizer
feature_names = clf.named_steps['tfidf'].get_feature_names_out()
# get the feature importance from the MultinomialNB classifier
#feature_importance = clf.named_steps['clf'].feature_log_prob_[0]
feature_importance_0 = clf.named_steps['clf'].feature_log_prob_[0]
feature_importance_1 = clf.named_steps['clf'].feature_log_prob_[1]

# create a dictionary with feature names and their importance values
features_1 = dict(zip(feature_names, feature_importance_1))
features_0 = dict(zip(feature_names, feature_importance_0))

sorted_features_0 = sorted(features_0.items(), key=lambda x: x[1], reverse=True)
sorted_features_1 = sorted(features_1.items(), key=lambda x: x[1], reverse=True)

for feature, importance in sorted_features_1[:5]:
    print(f"{feature}: {importance}")

for feature, importance in sorted_features_0[:5]:
    print(f"{feature}: {importance}:{np.exp(importance)}")

features_dict = {}
for feature, imp in sorted_features_0[:100]:
    features_dict[feature] = [imp, np.exp(imp)]

# Convert the dictionary to a Pandas DataFrame
df = pd.DataFrame.from_dict(features_dict, orient='index', columns=['importance', 'exp_importance'])

# Save the DataFrame to a CSV file
df.to_csv('output_0.csv')

features_dict = {}
for feature, imp in sorted_features_1[:100]:
    features_dict[feature] = [imp, np.exp(imp)]

# Convert the dictionary to a Pandas DataFrame
df = pd.DataFrame.from_dict(features_dict, orient='index', columns=['importance', 'exp_importance'])

# Save the DataFrame to a CSV file
df.to_csv('output_1.csv')

# Predict the test set results
y_pred = clf.predict(X_test)

# Generate confusion matrix
cm = confusion_matrix(y_test, y_pred)

# Print confusion matrix
print(cm)
sns.heatmap(cm, annot=True, fmt='d', cmap='Blues')
plt.title('Confusion matrix')
plt.xlabel('Predicted label')
plt.ylabel('True label')
plt.show()